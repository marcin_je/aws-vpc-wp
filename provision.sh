#!/bin/bash


cd vpc-provisioning


ansible-playbook provision.yml -i hosts -v


KIBANA=$(cat elk_instance_public_dns)
WP=$(cat wp_instance_public_dns)

echo "Once provisioned, you can access Kibana dashboard by accessing:"
echo "http://${KIBANA}:5601"


echo "Once provisioned, you can access WordPress dashboard by accessing:"
echo "http://${WP}"
