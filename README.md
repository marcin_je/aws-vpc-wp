# Provision VPC with ELK stack and WordPress 

## Introduction

This project demonstrated how to use Ansible to provision AWS VPC with ELK stack and WordPress running. It uses Ubuntu 14.04 LTE and docker containers to run most of the infrastructure. 

## Requirements for provisioning VPC:
Ansible, boto, python 2.7.10+, ~/.aws/credentials with keys allowing full admin access
Optionally: aws cli for destroying VPC

## Requirements for starting ELK docker containers locally:
docker 1.10+, docker-compose 

## Creating resources

In order to create VPC with instances required to run the stack, run:

./create-vpc.sh

In order to provision instaces (ELK, Sensu, WordPress) run:

./provision.sh

The above command will print URL to Kibana dashboard and WordPress blog


If you would like to deploy ELK stack on your local docker run:

./provision-elk-locally.sh


## What's monitored?
Syslogs of each instance are being redirected to logstash/kibana (rsyslog), index: logstash-* 
Instance resources are being redirected to elasticsearch/kibana (topbeat), index: topbeat-*

## Future monitoring improvements
The monitoring solution can be expanded by adding Sensu stack and collecting various metrics from each instance with help of Sensu plugins, then redirecting them to either ELK stack or Graphite/influxdb

## TODO

Automate creation of dashboards for topbeat:
curl -L -O http://download.elastic.co/beats/dashboards/beats-dashboards-1.2.3.zip
unzip beats-dashboards-1.2.3.zip
cd beats-dashboards-1.2.3/
./load.sh

## Logging into EC2 instances,

You can login into each instance by:

ssh ubuntu@public_ip

## Other considerations

At the moment all instances are in public subnet. It is ideal that instances which don't need to be public facing are in private subnet. That requires a NAT which would allow instances to access Internet and which would act as 'jump box'
